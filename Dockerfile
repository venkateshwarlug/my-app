FROM alpine:latest
RUN apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
LABEL AUTHOR="DEVOPS"
COPY ./target/my-app-1.0-SNAPSHOT.jar /my-app-1.0-SNAPSHOT.jar
CMD ["java", "-cp","my-app-1.0-SNAPSHOT.jar com.mycompany.app.App"]

